describe('Login Flow', function() {
  it('Logs in', function() {
    cy.visit('http://localhost:1234');

    cy.getByLabelText('User').type('admin');
    cy.getByLabelText('Password').type('admin123');

    cy.get('form').within(() => {
      cy.getByText('Login').click();
    });

    cy.getByText('App').should('exist');
  });
});

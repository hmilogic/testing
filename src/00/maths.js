export const sum = (a, b) => a + b;

export const difference = (a, b) => a - b;

export const multiplication = (a, b) => a * b;

export const division = (a, b) => {
  if (b === 0) return undefined;

  return a / b;
};

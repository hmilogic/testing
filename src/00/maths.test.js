import { sum, division, difference, multiplication } from './maths';

describe('maths', () => {
  it('adds values', () => {
    expect(sum(1, 2)).toEqual(3);
  });

  it('subtracts values', () => {
    expect(difference(5, 3)).toEqual(2);
  });

  it('multiplies values', () => {
    expect(multiplication(5, 3)).toEqual(15);
  });

  it('divides values', () => {
    expect(division(6, 2)).toEqual(3);
  });

  it('divides by zero', () => {
    expect(division(6, 0)).toEqual(undefined);
  });
});

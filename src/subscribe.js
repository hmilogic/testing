import * as React from 'react';

export function subscribe(Component) {
  return props => {
    const [activeDialogs, setActiveDialogs] = React.useState([
      'Dialog Content',
    ]);
    return (
      <Component
        {...props}
        activeDialogs={activeDialogs}
        onCloseDialog={() => setActiveDialogs([])}
      />
    );
  };
}

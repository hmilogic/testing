import * as React from 'react';

export function List(props) {
  const { selectedIndex = 0 } = props;

  return (
    <ol
      data-testid="list"
      onKeyUp={event => {
        // ...
      }}
    >
      {props.items.map((item, index) => {
        const isSelected = index === props.selectedIndex;

        return (
          <li
            key={index}
            tabIndex={0}
            data-selected={isSelected ? 'true' : undefined}
            style={{ outline: isSelected ? '1px solid red' : undefined }}
            onClick={() => props.onSelect(index)}
          >
            {item}
          </li>
        );
      })}
    </ol>
  );
}

import * as React from 'react';
import { fireEvent, render } from 'react-testing-library';

import { List } from './List';

describe('List', () => {
  it('renders items', () => {
    const { queryByText, getByTestId } = render(<List items={[1, 2, 3]} />);

    expect(queryByText('1')).not.toBeNull();
    expect(queryByText('2')).not.toBeNull();
    expect(queryByText('3')).not.toBeNull();
  });

  it('selects items', () => {
    const onSelect = jest.fn();
    const { getByText } = render(
      <List items={[1, 2, 3]} onSelect={onSelect} />,
    );

    const item = getByText('2');
    fireEvent.click(item);

    expect(onSelect).toHaveBeenCalledWith(1);
  });

  it('shows selected item', () => {
    const { getByText } = render(
      <List items={[1, 2, 3]} onSelect={() => {}} selectedIndex={1} />,
    );

    const item = getByText('2');
    expect(item.dataset.selected).toEqual('true');
  });
});

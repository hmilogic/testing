import { IntlProvider } from 'react-intl';

class App extends React.Component<Props> {
  static childContextTypes = {
    app: PropTypes.object,
  };

  getChildContext() {
    return { app: { listen: () => {} } };
  }

  render() {
    return this.props.children;
  }
}

export class LoadProductionDialog extends React.Component {
  componentWillMount() {}
  componentWillUnmount() {}
  componentDidUpdate() {}

  render() {
    return this.props.children;
  }
}

class Production extends React.Component {
  render() {
    return 'Production';
  }
}

export default props => {
  return (
    <LoadProductionDialog {...props}>
      <Production {...props} />
    </LoadProductionDialog>
  );
};

//  TEST

import { fireEvent, render } from 'react-testing-library';

import Production from './Production';

it('has dialogs', props => {
  const onPopDialog = jest.fn();

  const { getByText, unmount } = render(
    <IntlProvider>
      <LoadProductionDialog onPopDialog={onPopDialog} />
    </IntlProvider>,
  );

  expect(onPopDialog).toHaveBeenCalledWith(/* ... */);

  unmount();

  expect(onPopDialog).toHaveBeenLastCalledWith(/* ... */);
});

const productionProps = {
  // ...
};

it('works', props => {
  const { getByText } = render(
    <IntlProvider>
      <MockApp>
        <Production {...productionProps} />
      </MockApp>
    </IntlProvider>,
  );

  getByTestId('input');
  fireEvent.click(getByText('Run'));
});

const field /* ... */ = fireEvent.focus(field);

fireEvent.blur(field);

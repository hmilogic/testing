import * as React from 'react';

import { NumericInput } from './02/NumericInput';
import { List } from './03/List';
import { Dialog } from './06/Dialog';

import { subscribe } from './subscribe';

export function App(props) {
  const {
    values,
    selectedItem,
    setSelectedItem,
    setValues,
    activeDialogs,
    onCloseDialog,
  } = props;
  return (
    <div>
      <h1>App</h1>

      {activeDialogs.length > 0 ? (
        <Dialog onClose={onCloseDialog}>
          {activeDialogs[activeDialogs.length - 1]}
        </Dialog>
      ) : null}

      <List
        items={values}
        selectedIndex={selectedItem}
        onSelect={setSelectedItem}
      />

      <NumericInput
        value={values[selectedItem]}
        minimum={0}
        maximum={100}
        onChange={nextValue => {
          const nextValues = values.map((value, index) => {
            if (index === selectedItem) {
              return nextValue;
            }
            return value;
          });
          setValues(nextValues);
        }}
      />
    </div>
  );
}

export const SubscribedApp = subscribe(App);

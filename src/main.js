import * as React from 'react';
import { render } from 'react-dom';

import { Login } from './01/Login';
import { SubscribedApp } from './App';

function Main(props) {
  const [loggedIn, setLoggedIn] = React.useState(false);

  const [selectedItem, setSelectedItem] = React.useState(0);
  const [values, setValues] = React.useState([42, 45, 10, 20]);

  if (!loggedIn) {
    return (
      <>
        <h1>Login</h1>
        <Login onSubmit={() => setLoggedIn(true)} />
      </>
    );
  }

  return (
    <SubscribedApp
      values={values}
      selectedItem={selectedItem}
      setSelectedItem={setSelectedItem}
      setValues={setValues}
    />
  );
}

render(<Main />, document.getElementById('app'));

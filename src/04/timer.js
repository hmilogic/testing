export function autoLogout(duration, callback) {
  window.setTimeout(callback, duration);
}

export function asyncFunction() {
  return new Promise(resolve => {
    resolve(42);
  });
}

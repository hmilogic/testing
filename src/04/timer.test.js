import { autoLogout, asyncFunction } from './timer';

it('should auto-logout', () => {
  const onCallback = jest.fn();

  jest.useFakeTimers();

  autoLogout(2000, onCallback);

  jest.advanceTimersByTime(2000);

  expect(onCallback).toHaveBeenCalled();
});

it('should test async function', async () => {
  const value = await asyncFunction();
  expect(value).toEqual(42);
});

// API Hints
//
// jest.useFakeTimers();
// jest.advanceTimersByTime();
// jest.runAllTimers();

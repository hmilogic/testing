import * as React from 'react';

export function Dialog(props) {
  return (
    <div
      data-testid="dialog"
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: 'rgba(0,0,0,0.3)',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          background: '#FFF',
        }}
      >
        {props.children}
        <button onClick={props.onClose}>Close</button>
      </div>
    </div>
  );
}

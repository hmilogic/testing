import * as React from 'react';
import { render, fireEvent, within } from 'react-testing-library';

import { App, SubscribedApp } from '../App';

jest.mock('../subscribe', () => {
  return {
    subscribe: Component => {
      return props => {
        const React = require('react');

        const [activeDialogs, setActiveDialogs] = React.useState([
          'Mocked Content',
        ]);

        return (
          <Component
            {...props}
            activeDialogs={activeDialogs}
            onCloseDialog={() => setActiveDialogs([])}
          />
        );
      };
    },
  };
});

describe('Dialog', () => {
  it('should open dialog', () => {
    const { queryByTestId, rerender } = render(
      <App
        values={[]}
        selectedItem={undefined}
        setSelectedItem={() => {}}
        setValues={() => {}}
        activeDialogs={[]}
      />,
    );

    // No dialog should be visible
    expect(queryByTestId('dialog')).toBeNull();

    rerender(
      <App
        values={[]}
        selectedItem={undefined}
        setSelectedItem={() => {}}
        setValues={() => {}}
        activeDialogs={['Dialog Content']}
      />,
    );

    // Dialog should now be visible
    expect(queryByTestId('dialog')).not.toBeNull();
  });

  it('should open dialog', () => {
    const { getByTestId, queryByText } = render(
      <SubscribedApp
        values={[]}
        selectedItem={undefined}
        setSelectedItem={() => {}}
        setValues={() => {}}
      />,
    );

    expect(queryByText('Mocked Content')).not.toBeNull();

    const dialog = getByTestId('dialog');
    fireEvent.click(within(dialog).getByText('Close'));

    expect(queryByText('Mocked Content')).toBeNull();
  });
});

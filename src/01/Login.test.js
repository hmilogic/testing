import * as React from 'react';
import { fireEvent, render } from 'react-testing-library';

import { Login } from './Login';

it('submits', () => {
  const onSubmit = jest.fn();

  const { getByLabelText, getByText } = render(<Login onSubmit={onSubmit} />);

  const userField = getByLabelText('User');
  fireEvent.change(userField, { target: { value: 'admin' } });
  const passwordField = getByLabelText('Password');
  fireEvent.change(passwordField, { target: { value: '12345' } });
  const submitButton = getByText('Login');

  fireEvent.click(submitButton);

  expect(onSubmit).toHaveBeenCalledWith('admin', '12345');
});

it('can only submit when username and password were entered', () => {
  const onSubmit = jest.fn();

  const { getByText } = render(<Login onSubmit={onSubmit} />);

  fireEvent.click(getByText('Login'));

  expect(onSubmit).not.toHaveBeenCalled();
});

it('disables the login button when no credentials have been entered', () => {
  const { getByText } = render(<Login onSubmit={() => {}} />);

  expect(getByText('Login').disabled).toEqual(true);
});

it('can cancel login', () => {
  const onCancel = jest.fn();

  const { getByText } = render(<Login onCancel={onCancel} />);

  fireEvent.click(getByText('Cancel'));

  expect(onCancel).toHaveBeenCalled();
});

// API Hints
//
// render
// getByLabelText
// fireEvent
// expectToHaveBeenCalled

import * as React from 'react';

export function Login(props) {
  const [userName, setUserName] = React.useState('');
  const [password, setPassword] = React.useState('');

  return (
    <form
      onSubmit={event => {
        event.preventDefault();
        const elements = event.target.elements;

        props.onSubmit(elements.user.value, elements.password.value);
      }}
    >
      <label>
        User
        <input
          value={userName}
          onChange={event => setUserName(event.target.value)}
          id="user"
        />
      </label>

      <label>
        Password
        <input
          value={password}
          onChange={event => setPassword(event.target.value)}
          id="password"
          type="password"
        />
      </label>

      <button type="button" onClick={() => props.onCancel && props.onCancel()}>
        Cancel
      </button>
      <button disabled={userName.length === 0 || password.length === 0}>
        Login
      </button>
    </form>
  );
}

import * as React from 'react';

/**
 * User input component for entering a numeric float value (e.g. `42.5`).
 */
export function NumericInput(props) {
  const [userValue, setUserValue] = React.useState(props.value);

  const postChange = nextValue => {
    if (!isFinite(nextValue)) return;

    if (props.minimum != null && nextValue < props.minimum) return;
    if (props.maximum != null && nextValue > props.maximum) return;

    props.onChange(nextValue);
  };

  const onChange = event => {
    const nextValue = event.target.value;

    setUserValue(nextValue);

    if (nextValue.length === 0) return;

    const inputValue = Number(nextValue);
    postChange(inputValue);
  };

  return (
    <input
      type="text"
      onKeyDown={event => {
        if (event.key === 'ArrowUp') {
          postChange(props.value + 1);
        }

        if (event.key === 'ArrowDown') {
          postChange(props.value - 1);
        }
      }}
      value={userValue}
      onChange={onChange}
      onBlur={() => {
        setUserValue(props.value);
      }}
    />
  );
}

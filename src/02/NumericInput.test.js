import * as React from 'react';
import { fireEvent, render } from 'react-testing-library';

import { NumericInput } from './NumericInput';

describe('NumericInput', () => {
  it('shows value', () => {
    const { queryByValue } = render(<NumericInput value={42} />);

    expect(queryByValue('42')).not.toEqual(null);
  });

  it('receives onChange events', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput value={42} onChange={onChange} />,
    );

    const field = getByValue('42');
    fireEvent.change(field, { target: { value: 45 } });

    expect(onChange).toHaveBeenCalledWith(45);
  });

  it('handles incorrect input', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput value={42} onChange={onChange} />,
    );

    const field = getByValue('42');
    fireEvent.change(field, { target: { value: 'foo' } });

    expect(onChange).not.toHaveBeenCalled();
  });

  it('only accepts values within limits', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput minimum={0} maximum={100} value={42} onChange={onChange} />,
    );

    const field = getByValue('42');
    fireEvent.change(field, { target: { value: '-30' } });
    fireEvent.change(field, { target: { value: '130' } });
    expect(onChange).not.toHaveBeenCalled();
  });

  it('can use keyboard to increment value', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput minimum={0} maximum={100} value={42} onChange={onChange} />,
    );

    const field = getByValue('42');
    fireEvent.keyDown(field, { key: 'ArrowUp' });

    expect(onChange).toHaveBeenCalledWith(43);
  });

  it('can use keyboard to decrement value', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput minimum={0} maximum={100} value={42} onChange={onChange} />,
    );

    const field = getByValue('42');
    fireEvent.keyDown(field, { key: 'ArrowDown' });

    expect(onChange).toHaveBeenCalledWith(41);
  });

  it('can use keyboard to increment/decrement value (within limits)', () => {
    const onChange = jest.fn();
    const { getByValue, rerender } = render(
      <NumericInput minimum={0} maximum={1} value={1} onChange={onChange} />,
    );

    const field = getByValue('1');
    fireEvent.keyDown(field, { key: 'ArrowUp' });

    rerender(
      <NumericInput minimum={0} maximum={1} value={0} onChange={onChange} />,
    );

    fireEvent.keyDown(field, { key: 'ArrowDown' });

    expect(onChange).not.toHaveBeenCalled();
  });

  it('accepts partial inputs', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput value={1} onChange={onChange} />,
    );

    const field = getByValue('1');
    fireEvent.change(field, { target: { value: '-' } });

    expect(onChange).not.toHaveBeenCalled();
    expect(field.value).toEqual('-');

    fireEvent.change(field, { target: { value: '-30' } });
    expect(onChange).toHaveBeenCalledWith(-30);
  });

  it('resets invalid values on blur', () => {
    const { getByValue } = render(
      <NumericInput value={1} onChange={() => {}} />,
    );

    const field = getByValue('1');
    fireEvent.change(field, { target: { value: 'invalid' } });

    fireEvent.blur(field);
    expect(field.value).toEqual('1');
  });

  it('treats empty string as invalid', () => {
    const onChange = jest.fn();
    const { getByValue } = render(
      <NumericInput value={1} onChange={onChange} />,
    );

    const field = getByValue('1');
    fireEvent.change(field, { target: { value: '' } });

    expect(onChange).not.toHaveBeenCalled();
  });
});

import * as React from 'react';
import { render } from 'react-testing-library';

import { App } from './Route';

jest.mock('react-router', () => {
  return {
    Route: props => <span>{props.render()}</span>,
  };
});

it('renders content', () => {
  const { queryByText } = render(<App />);
  expect(queryByText('App Content')).not.toBeNull();
});

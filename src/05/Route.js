import * as React from 'react';

import { Route } from 'react-router';

export function App() {
  return <Route exact path="/" render={() => <div>App Content</div>} />;
}

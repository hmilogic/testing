import * as React from 'react';

function isMobileLayout(windowWidth) {
  return windowWidth <= 800;
}

export const App = () => {
  const width = window.innerWidth;

  return <>{width > 800 ? <Desktop /> : <Mobile />}</>;
};
